# Sauteed Onions
Sauteed onions associate registered domain names with onion addresses.  These
associations are established in TLS certificates, making them publicly
enumerable in append-only CT logs.

One of the most prominent use-cases of sauteed onions is to help users defeat
censorship of TLS sites: onionsites can be used _if they are discoverable_,
which is what sauteed onions help with.  This tightens the relation between
registered domain names, HTTPS, and onionsites.

Refer to [www.sauteed-onions.org][] for quick-start instructions, an FAQ, and
our pre-printed paper.

[www.sauteed-onions.org]: https://www.sauteed-onions.org
