package main

import (
	"fmt"

	"github.com/google/certificate-transparency-go"
	"github.com/google/certificate-transparency-go/asn1"
	"github.com/google/certificate-transparency-go/x509/pkix"
)

func SANsFromLogEntry(entry *ct.LogEntry) ([]string, error) {
	if entry.Precert == nil && entry.X509Cert == nil {
		return nil, fmt.Errorf("parse: no (pre)cert in log entry")
	}
	if entry.Precert != nil && entry.X509Cert != nil {
		return nil, fmt.Errorf("parse: both precert and cert in log entry")
	}

	var exts []pkix.Extension
	if entry.Precert != nil {
		exts = entry.Precert.TBSCertificate.Extensions
	}
	if entry.X509Cert != nil {
		exts = entry.X509Cert.Extensions
	}

	var sans []string
	for _, ext := range exts {
		if ext.Id.Equal(asn1.ObjectIdentifier{2, 5, 29, 17}) {
			moreSANs, err := extract(ext)
			if err != nil {
				return nil, fmt.Errorf("parse: %v", err)
			}

			sans = append(sans, moreSANs...)
		}
	}
	return sans, nil
}

// extract is mostly a copy-paste from Andrew Ayer's CertSpotter, thank you.  See:
// https://github.com/SSLMate/certspotter/blob/54f34077d3bebe8aafce07dcfbffeb928c6e1d04/x509.go#L380
func extract(extSAN pkix.Extension) ([]string, error) {
	var seq asn1.RawValue
	rest, err := asn1.Unmarshal(extSAN.Value, &seq)
	if err != nil {
		return nil, fmt.Errorf("failed to parse subjectAltName extension: %v", err)
	}
	if len(rest) != 0 {
		// Don't complain if the SAN is followed by exactly one zero byte, which is a common error.
		if !(len(rest) == 1 && rest[0] == 0) {
			return nil, fmt.Errorf("trailing data in subjectAltName extension: %v", rest)
		}
	}
	if !seq.IsCompound || seq.Tag != 16 || seq.Class != 0 {
		return nil, fmt.Errorf("failed to parse subjectAltName extension: bad SAN sequence")
	}

	sans := []string{}
	buf := seq.Bytes
	for len(buf) > 0 {
		var val asn1.RawValue
		var err error
		buf, err = asn1.Unmarshal(buf, &val)
		if err != nil {
			return nil, fmt.Errorf("failed to parse subjectAltName extension item: %v", err)
		}

		sans = append(sans, string(val.Bytes))
	}
	return sans, nil
}
