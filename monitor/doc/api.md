# Sauteed onion search API, v0

This document describes a restful HTTP API that permits clients to query a
sauteed onion data set. It is assumed that a back-end continuously downloads
all sauteed onion certificates from well-known CT logs, see [follow-go][] for a
basic example.

Please refer to www.sauteed-onions.org and our [pre-printed paper][] for
background.

**Warning:**
this is a work-in-progress draft.

[follow-go]: https://gitlab.torproject.org/rgdd/sauteed-onions/-/tree/main/monitor/follow-go
[pre-printed paper]: https://www.sauteed-onions.org/doc/paper.pdf

## 1 - Overview

A sauteed onion search API has one or more well-known URLs that are shared
across all public endpoints. Example URLs:

- http://qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion/some/path
- https://example.org/some/path

HTTP requests encode input parameters with percent-encoding, also known as URL
encoding.

The data in HTTP responses are JSON-encoded on success with HTTP status 200 OK.
Otherwise a different HTTP status code is returned and the body contains a
human-readable error message.

A client can query for all onion addresses associated with a given domain name
using the search endpoint in Section 2.1. A client may verify that the
provided answers are based on CT-logged certificates using the get endpoint in
Section 2.2. In other words, this API functions much like a search engine but
is verifiable.

It is not within scope of the v0 API to prove that no domain name and
associated onion address was left out from an answer, see [pre-printed paper]
on how to achieve that.

## 2 - Public endpoints

### 2.1 - search

    GET url/search?in=example.org

Input:

- `in`: domain name to look up onion addresses for (exact match)

Output:

- A list with zero or more objects with the following key-value pairs:
  - `domain_name` --> string (matches what was specified by `in`)
  - `onion_addr` --> string (an onion address associated with `domain_name`)
  - `identifiers` --> []string (unique identifiers to access auxiliary
    certificate information)

There are multiple identifiers because a domain name and an associated onion
address may exist in several CT-logged certificates.

Example:

    $ curl --data-urlencode "in=www.sauteed-onions.org" https://example.org
    [
      {
        "domain_name": "www.sauteed-onions.org",
        "onion_addr": "qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion",
        "identifiers": [
          "2009",
          "4711"
        ]
      }
    ]

### 2.2 - get

    GET url/get?id=xxxxxxxx

Input:

- `id`: an identifier to locate auxiliary certificate information

Output:

- `domain_name` --> string
- `onion_addr` --> string (an onion address associated with `domain_name`)
- `log_id` --> string (defined in RFC 6962, §3.2; base64-encoded)
- `log_index` --> uint64 (log index to locate the sauteed onion certificate)
- `cert_path` --> string (relative path to `url` for downloading the sauteed
  onion certificate)

A relative path to download the sauteed onion certificate is included for
clients that wish to verify that a CA-issued certificate exists without having
to touch a CT log. The certificate served on this relative path is PEM-encoded.

On an unknown identifier HTTP status 404 Not Found is returned.

Example:

    $ curl --data-urlencode "id=2009" https://example.org
    {
      "domain_name": "www.sauteed-onions.org",
      "onion_addr": "qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion",
      "log_id": "KXm+8J45OSHwVnOfY6V35b5XfZxgCvj5TV0mXCVdx4Q=",
      "log_index": "1042765777",
      "cert_path": "/example-path"
    }
