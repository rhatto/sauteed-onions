# Web extension

## Setup

System that this web extension was developed on:

    $ lsb_release -a
    No LSB modules are available.
    Distributor ID: Debian
    Description:    Debian GNU/Linux 10 (buster)
    Release:        10
    Codename:       buster

    $ npm -v
    6.14.15

    $ node --version
    v14.18.2

    $ vue --version
    @vue/cli 4.5.15

    $ web-ext --version
    6.6.0

Web extension template was generated based on instructions
[here](https://github.com/Kocal/vue-web-extension/tree/v1).

The important parts to get started:

    $ vue init kocal/vue-web-extension#v1 webext
    $ cd webext
    $ npm install

At some point we should fix the warnings that `npm install` cries about. Any
help from someone that is more familiar with this environment would be helpful.

## Development

Terminal one:

    npm run watch:dev

Terminal two:

    web-ext run --browser-console -s dist

You can also install temporary in Firefox as follows:

1. `npm run build:dev && npm run build-zip`
2. Go to about:debugging ->
   This Firefox ->
   Load Temporary Add-on ->
   specify the generated zip file in `dist-zip`

Example on how to add a new dependency:

    npm install asn1js
