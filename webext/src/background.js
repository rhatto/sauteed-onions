const { SHA3 } = require('sha3');
const base32 = require('base32.js');
import * as asn1js from 'asn1js';
import { Certificate, verifySCTsForCertificate } from 'pkijs';

var onionLocations = {};
var gCtLogs;

console.log('Begin loading.');

function toArrayBuffer(der) {
  const buf = Buffer.from(der);
  const ab = new ArrayBuffer(buf.length);
  const view = new Uint8Array(ab);
  for (let i = 0; i < buf.length; ++i) {
    view[i] = buf[i];
  }
  return ab;
}

const getX509Ext = (extensions, v) => {
  for (var extension in extensions) {
    if (extensions[extension].extnID === v) {
      return extensions[extension];
    }
  }

  return {
    extnValue: undefined,
    parsedValue: undefined,
  };
};

// Stolen from satis-selfauth-domains/webext/lib/log.js
function log_assert(cond, msg) {
  if (!cond) {
    msg = msg || 'Assertion failed';
    console.assert(cond, msg);
  }
  return cond;
}

// Adapted from satis-selfauth-domains/webext/lib/onion.js
function onion_v3decode(onionstr) {
  // "Assert" on these things because they should have been validated already
  if (!log_assert(onionstr.length == 56, 'Invalid v3 onion string (length)')) return null;
  let bytes = new base32.Decoder().write(onionstr).finalize();
  if (!log_assert(bytes.length == 35, 'Invalid v3 onion string (bytes length)')) return null;
  let pubkey = bytes.slice(0, 32);
  let checksum1 = bytes.slice(32, 34);
  let version = bytes.slice(34);
  // Last byte (version) must be three
  if (version != 3) {
    log_debug('Invalid version', bytes.slice(-1));
    return null;
  }
  // Checksum must be valid
  let checksum2 = new SHA3(256);
  checksum2
    .update('.onion checksum')
    .update(Buffer.from(pubkey))
    .update(Buffer.from(version));
  checksum2 = checksum2.digest().slice(0, 2);
  if (checksum1[0] != checksum2[0] || checksum1[1] != checksum2[1]) {
    log_debug('Invalid v3 onion string (checksum)', checksum1, checksum2);
    return null;
  }
  return {
    pubkey: pubkey,
    checksum: checksum1,
    version: version,
    str: onionstr,
  };
}

function isOnionAddress(addr) {
  return addr != null && !!onion_v3decode(addr);
}

function getSauteedOnionAddress(addrs, origin) {
  let origin_subs = origin.split('.');
  for (let addr in addrs.altNames) {
    let altName = addrs.altNames[addr].value;
    let potential_sauteed_subs = altName.split('.');
    const onion = 'onion';

    if (origin_subs.length + 1 != potential_sauteed_subs.length) {
      continue;
    }

    let mismatch = false;
    for (let i = 0; i < origin_subs.length; ++i) {
      if (origin_subs[i] != potential_sauteed_subs[i + 1]) {
        mismatch = true;
        break;
      }
    }
    if (mismatch) {
      continue;
    }

    if (!potential_sauteed_subs[0].endsWith(onion)) {
      continue;
    }

    let end_idx = potential_sauteed_subs[0].length - onion.length;
    return potential_sauteed_subs[0].substring(0, end_idx);
  }

  return null;
}

function getOrigin(url) {
  const httpsScheme = 'https://';
  if (!url.startsWith(httpsScheme)) {
    return null;
  }

  const firstSlash = url.indexOf('/', httpsScheme.length);
  const firstColon = url.indexOf(':', httpsScheme.length);

  let origin = '';
  let port = '-1';
  let path = '';

  if (firstColon != -1) {
    origin = url.substring(httpsScheme.length, firstColon);
    if (firstSlash != -1) {
      port = url.substring(firstColon, firstSlash);
    } else {
      port = url.substring(firstColon);
    }
  } else if (firstSlash != -1) {
    origin = url.substring(httpsScheme.length, firstSlash);
    path = url.substring(firstSlash);
  } else {
    origin = url.substring(httpsScheme.length);
  }
  return { origin: origin, port: port, path: path };
}

function flattenCTLogs(raw_logs) {
  let logs = [];
  for (let opIdx in raw_logs.operators) {
    let operator = raw_logs.operators[opIdx];
    for (let server in operator.logs) {
      let log = {};
      log['operator'] = operator.name;
      log['description'] = operator.logs[server].description;
      log['log_id'] = operator.logs[server].log_id;
      log['key'] = operator.logs[server].key;
      log['url'] = operator.logs[server].url;
      log['mmd'] = operator.logs[server].mmd;
      // TODO check start/end intervals

      logs.push(log);
    }
  }

  console.log('Logs: ', logs);
  return logs;
}

// https://groups.google.com/a/chromium.org/g/ct-policy/c/IdbrdAcDQto
async function loadCTLogs() {
  const filename = 'log_list.json';
  const url = browser.runtime.getURL(filename);
  const response = await fetch(url);
  const parsed = await response.json();
  return await flattenCTLogs(parsed);
}

browser.pageAction.onClicked.addListener(details => {
  if (details.id in onionLocations) {
    browser.tabs
      .update(details.id, {
        url: onionLocations[details.id],
      })
      .then(
        tab => {
          console.log(`successfully changed tab (${tab}) location to: ${onionLocations[details.id]}.`);
        },
        e => {
          console.log(`tab location not changed: ${e}`);
        }
      );
  }
});

browser.webRequest.onCompleted.addListener(
  async function(details) {
    if (details.tabId in onionLocations) {
      console.log('Showing page action');
      browser.pageAction.show(details.tabId);
    }
  },
  { urls: ['<all_urls>'] }
);

browser.webRequest.onHeadersReceived.addListener(
  async function(details) {
    // Should "onion available" only in the main frame associated with a tab
    if (details.frameId != 0 || details.tabId == -1 || details.type != 'main_frame') {
      return;
    }

    // Require that CT logs list is initialized.
    if (gCtLogs === undefined) {
      return;
    }

    if (details.tabId in onionLocations) {
      delete onionLocations[details.tabId];
    }

    let origin = getOrigin(details.url);
    // Invalid url or non-https url
    if (origin == null) {
      return;
    }

    let si = await browser.webRequest.getSecurityInfo(details.requestId, {
      certificateChain: true,
      rawDER: true,
    });

    const asn1 = asn1js.fromBER(toArrayBuffer(si.certificates[0].rawDER));
    let x509 = new Certificate({ schema: asn1.result });
    let san = getX509Ext(x509.extensions, '2.5.29.17').parsedValue;

    console.log('san: ', san);

    let onionAddr = getSauteedOnionAddress(san, origin.origin);

    const issuerAsn1 = asn1js.fromBER(toArrayBuffer(si.certificates[0].rawDER));
    let issuerX509 = new Certificate({ schema: asn1.result });

    console.log('san: ', san);

    // https://apowers313.github.io/pkijs-docs-test/function/index.html#static-function-verifySCTsForCertificate
    // verifySCTsForCertificate(x509, /* issuerCert */, /* logs */, -1 /* index within list */);

    let verifyResult = verifySCTsForCertificate(x509, issuerX509, gCtLogs);
    if (!verifyResult) {
      console.log(`SCT verification failed: ${origin.origin}`);
      return;
    }
    console.log(`SCT verification succeeded: ${origin.origin}`);

    if (isOnionAddress(onionAddr)) {
      console.log('Caching onion address');
      // TODO How should we determine url scheme?
      onionLocations[details.tabId] = `http://${onionAddr}.onion${origin.path}`;
    }
  },
  { urls: ['<all_urls>'] },
  ['blocking']
);

loadCTLogs().then(result => {
  gCtLogs = result;
  console.log('loaded');
});
