# About
Sauteed onions is an ongoing research project.  The core contributors are:

  - Rasmus Dahlberg (Karlstad University)
  - Paul Syverson (U.S. Naval Research Laboratory)
  - Linus Nordberg (Verkligen Data AB)
  - Matthew Finkel (The Tor Project)

Any contributions to further develop sauteed onions would be most welcome.

Get in touch via IRC or find us somewhere on the internet.
