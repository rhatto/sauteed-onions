# Frequently asked questions
Below is a list of questions that are (or will be) frequently asked.

## What are sauteed onions?

Essentially any TLS setup where the certificate contains an onion address as a
subdomain SAN.  The SAN identifying a _sauteed onion certificate_ for
`www.example.com` is:

	<onion addr>onion.www.example.com

## What do sauteed onions do?

It makes an association from a registered domain name to an onion address
as transparent as a site's TLS certificate.  This is useful to reliably discover
onion addresses of TLS sites.

## Why are they called sauteed onions?

It is a cooking analogy.  An onion that is sauteed becomes transparent.

## Who uses sauteed onions?

An [enumeration][] based on a few CT logs is available, updated hourly.

[enumeration]: https://www.sauteed-onions.org/db/index

## How do I set up an onion service?

The Tor Project provides a [guide for setting up an onion service][].

You may also be interested in Alec Muffett's Enterpise Onion Toolkit ([EOTK][]).
It is a tool that deploys onion service access for existing websites.

[guide for setting up an onion service]: https://community.torproject.org/onion-services/setup/
[EOTK]: https://github.com/alecmuffett/eotk/

## Are .onion addresses part of the TLS ecosystem?

Yes, the CA/B forum voted to [allow domain validation of .onion addresses][] in
February, 2020.  Obtaining a TLS certificate with a registered domain name and a
`.onion` address is not controversial and supported by [DigiCert][] and
[HARICA][].

[DigiCert]: https://www.digicert.com/blog/ordering-a-onion-certificate-from-digicert
[HARICA]: https://news.harica.gr/article/onion_announcement/

The main reasons for defining sauteed onions based on onion addresses as
subdomains are usability and robustness.  Any of today's CAs can be used.

Of note is that no additional certificates are issued for existing TLS sites,
but CAs need to verify one more registered domain name.  When compared to an
actual `.onion` address, CT logs need to store a handful of extra bytes per
sauteed onion certificate.  We argue that this is a reasonable "hack" when
weighted towards the benefit and intended use-case: a good mechanisms to
discover onion addresses _for TLS sites that opted-in_ to resist censorship.

[allow domain validation of .onion addresses]: https://cabforum.org/2020/02/20/ballot-sc27v3-version-3-onion-certificates/

## Are sauteed onions replacing .onion addresses?

No, you still need to visit an onionsite via Tor.  If you are interested in
the use of onion addresses for self-authentication and hijack
resistance in browsers without Tor access, see the work of
[Syverson, Finkel, Eskandarian, and Boneh][].

[Syverson, Finkel, Eskandarian, and Boneh]: https://doi.org/10.1145/3463676.3485610


## Are sauteed onions replacing .onion certificates?

No, you need to obtain a certificate with a `.onion` address to get [HTTPS to
your onion site][].

[HTTPS to your onion site]: https://community.torproject.org/onion-services/advanced/https/

## Are sauteed onions replacing Onion-Location?

No, but "certificate-based onion location" via sauteed onions could be a future
improvement.  For example, it would be difficult to claim association with
someone else's onion address without detection.  It also works for use-cases of
[Onion-Location][] that are "not web".

We prototyped a [web extension][] that implements certificate-based
Onion-Location.

[web extension]: TODO:link

[Onion-Location]: https://community.torproject.org/onion-services/advanced/onion-location/

## How can I contribute to sauteed onions?

If you have a registered domain name and an associated onion address, setup
sauteed onions.

If you run a CT monitor, consider setting it up as an onion service to make it
harder to censor.  Bonus points if sauteed onions is setup too, or if a UX
tailored for sauteed onions is added.

Reach out to say hello, provide feedback, or help with any next steps.

## What are the next steps?

  1. Stand up a search service that has a UX tailored for sauteed onions.  For
     example, the above [enumeration][] and any direct search results could be
     less verbose to simplify usage.
  2. Relax trust in the search service, ranging from simple control queries to
     applying further transparency log patterns and potentially even integrating
     with Tor's DHT in the long run.
  3. Prototype sauteed onion features that could eventually land in Tor
     Browser.  For example:
     - Onion-Location can be based on sauteed onion certificates.
     - Searching can be supported natively with verification of all answers.
     - Sauteed onion rulesets can be derived automatically, similar to
       [SecureDrop names][].

Further sketches and motivations of sauteed onions are available in our
pre-printed paper.
     
[SecureDrop names]: https://securedrop.org/faq/getting-onion-name-your-securedrop/
